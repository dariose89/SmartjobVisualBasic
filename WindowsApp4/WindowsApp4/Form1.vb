﻿
Imports MySql.Data.MySqlClient

Public Class Form1
    Dim regis As Integer
    Dim dclick As Boolean

    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=localhost;user=root;password=;database=prueba;Convert Zero Datetime=True"
        conexion.Open()


        Me.Text = "Menú principal"
        Button1.Text = "Stock"
        Button2.Text = "Clientes"
        Button3.Text = "Ingresos y  Egresos"
        GroupBox1.Text = "Planillas"
        GroupBox2.Text = "Agenda"
        Label1.Text = ""
        Button4.Text = "Nuevo"
        Button5.Text = "Modificar"
        Button6.Text = "Eliminar"
        CheckBox1.Text = "Prioridad"
        DataGridView1.RowTemplate.Height = 50


        leer()
        DataGridView1.AllowUserToAddRows = False
        colorea()
    End Sub
    Private Sub limpiar()
        Label1.Text = ""
        TextBox1.Text = ""
        RichTextBox1.Text = ""
    End Sub
    Private Sub Form1_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        limpiar()
    End Sub
    Public Sub leer()
        dclick = False
        Dim consulta As String
        consulta = "SELECT* from agenda"
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, "agenda")
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = "agenda"
        Me.DataGridView1.Columns(1).DefaultCellStyle.Format = "dd/MM/yyyy"
        DataGridView1.Columns(0).Width = 50
        DataGridView1.Columns(1).Width = 100
        DataGridView1.Columns(2).Width = 510
        DataGridView1.Columns(3).Width = 34

        DataGridView1.RowTemplate.Height = 50
        DataGridView1.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 10)
        DataGridView1.Sort(DataGridView1.Columns(0), System.ComponentModel.ListSortDirection.Descending)
        colorea()
    End Sub
    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If
        Dim renglon As Integer : renglon = e.RowIndex
        DataGridView1.Rows(renglon).Selected = True
        If Not IsDBNull(DataGridView1.Item(2, renglon).Value) Then
            RichTextBox1.Text = DataGridView1.Item(2, renglon).Value
            TextBox1.Text = DataGridView1.Item(0, renglon).Value
            dclick = True
            regis = renglon  'me dice en que reglon guarde'
        End If
        colorea()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Form2.Show()

    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Form3.Show()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Form4.Show()

    End Sub
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click

        If dclick = False Then
            MsgBox("Haga click en una agenda")
        Else
            Dim s As Integer
            s = MsgBox("¿Borrar agenda?", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                eliminar = "DELETE FROM agenda WHERE id='" & TextBox1.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
                limpiar()
                leer()
            End If
        End If

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim v As String

        v = validar1()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If
        If dclick = False Then
            MsgBox("Haga click en una agenda")

        Else
            Dim tipo1 As String
            '  Dim actualizar As String

            If CheckBox1.Checked = True Then tipo1 = "P" Else tipo1 = "N"

            'actualizar = "UPDATE  agenda SET tipo= '" & tipo1 & "agenda='" & RichTextBox1.Text & "'WHERE id='" & TextBox1.Text & "'"
            Dim actualizar As New MySqlCommand("UPDATE  agenda SET tipo=@tipo, agenda=@agenda WHERE id='" & TextBox1.Text & "'", conexion)
            actualizar.Parameters.AddWithValue("@tipo", tipo1)
            actualizar.Parameters.AddWithValue("@agenda", RichTextBox1.Text)

            ' comandos = New MySqlCommand(actualizar, conexion)
            actualizar.ExecuteNonQuery()

            MsgBox("Agenda modificada")
            limpiar()
            leer()
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim v As String
        Dim registro1 As String = 0
        v = validar()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If

        comandos = New MySqlCommand("INSERT INTO agenda (fecha,agenda,tipo)" & Chr(13) &
                                    "VALUES(@fecha,@agenda,@tipo )", conexion)

        Dim tipos As String

        If CheckBox1.Checked = True Then tipos = "P" Else tipos = "N"
        comandos.Parameters.AddWithValue("@agenda", RichTextBox1.Text)
        comandos.Parameters.AddWithValue("@tipo", tipos)
        comandos.Parameters.AddWithValue("@fecha", DateTimePicker1.Value)
        comandos.ExecuteNonQuery()


        MsgBox("¿Guardar nueva agenda?")
        limpiar()
        leer()

    End Sub
    Private Function validar1() As String
        validar1 = ""

    End Function

    Private Function validar() As String
        validar = ""

    End Function

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
    Private Sub colorea() 'colorea agenda con prioridad

        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Rows(i).Cells(3).Value Like "P" Then
                Me.DataGridView1.Rows(i).Cells(2).Style.ForeColor = Color.Red
                Me.DataGridView1.Rows(i).Cells(0).Style.BackColor = Color.Red
                Me.DataGridView1.Rows(i).Cells(1).Style.BackColor = Color.Red
                Me.DataGridView1.Rows(i).Cells(3).Style.BackColor = Color.Red

            End If
        Next
    End Sub
End Class
