﻿Imports MySql.Data.MySqlClient

Public Class Form3
    ' datagridview (1) label(3) textbox(3) button(4) radiobutton(2)
    ' este programa pide llenar con los datos y luego los graba en un archivo de texto
    Dim dato(2, 11) As String
    Dim renglon1 As Integer
    Dim renglon2 As String

    Dim regis As Integer
    Dim dclick As Boolean

    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet
    Dim datos1 As DataTable
    Dim datos2 As DataTable
    Private Sub Form3_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=localhost;user=root;password=;database=prueba;Convert Zero Datetime=True"
        conexion.Open()
        Me.Text = "Ingresos y Egresos"

        Label1.Text = "Hasta"
        Label2.Text = "Concepto"
        Label3.Text = "Tipo"
        Label4.Text = "Importe"
        Label5.Text = "Desde"
        Label6.Text = ""
        Label7.Text = "Cantidad"
        Label9.Text = "Productos"
        Label8.Text = ""
        Label10.Text = ""
        Label11.Text = "Stock Actual"
        Button1.Text = "Nuevo"
        Button2.Text = "Modificar"
        Button3.Text = "Borrar"
        Button4.Text = "Total"
        Button5.Text = "Volver"
        RadioButton1.Text = "Ingreso"
        RadioButton2.Text = "Egreso"
        GroupBox1.Text = "Informe"
        GroupBox2.Text = "Registro"

        Carga_combo2()
        leer()
        DataGridView1.AllowUserToAddRows = False
        colorea()
    End Sub
    Private Sub Carga_combo2()

        Dim consulta1 As String
        consulta1 = "SELECT * FROM stock ORDER BY producto"
        adaptador = New MySqlDataAdapter(consulta1, conexion)
        datos1 = New DataTable
        adaptador.Fill(datos1)
        With ComboBox2
            .DataSource = datos1
            .DisplayMember = "producto"
            .ValueMember = "id"
        End With
    End Sub

    Private Sub ComboBox2_TextChanged(sender As Object, e As EventArgs) Handles ComboBox2.TextChanged
        If ComboBox2.ValueMember.ToString <> "" Then
            Dim consulta2 As String
            consulta2 = "SELECT * FROM stock WHERE id=" + ComboBox2.SelectedValue.ToString() + ""
            adaptador = New MySqlDataAdapter(consulta2, conexion)
            datos2 = New DataTable
            adaptador.Fill(datos2)
            For Each row As DataRow In datos2.Rows
                TextBox2.Text = row("producto").ToString
                Label10.Text = row("stock")
                Label8.Text = row("id")
            Next
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim v As String
        v = validar()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If

        Dim tipo As String : tipo = "E"
        If RadioButton1.Checked = True Then
            tipo = "I"
        End If

        comandos = New MySqlCommand("INSERT INTO ingresos (fecha,concepto,tipo,importe)" & Chr(13) &
                                    "VALUES(@fecha,@concepto,@tipo,@importe )", conexion)

        comandos.Parameters.AddWithValue("@fecha", DateTimePicker1.Value)
        comandos.Parameters.AddWithValue("@concepto", TextBox2.Text)
        comandos.Parameters.AddWithValue("@tipo", tipo)
        comandos.Parameters.AddWithValue("@importe", TextBox3.Text)
        comandos.ExecuteNonQuery()

        Dim res As String
        res = Label8.Text
        If res <> "" Then
            Dim tipona As Integer

            tipona = Label10.Text

            If RadioButton1.Checked = True Then
                tipona = tipona - TextBox4.Text
            End If
            If RadioButton2.Checked = True Then
                tipona = tipona + TextBox4.Text
            End If

            Dim actualizar3 As New MySqlCommand("UPDATE  stock SET  stock=@stock WHERE id='" & Label8.Text & "'", conexion)

            actualizar3.Parameters.AddWithValue("@stock", tipona)

            actualizar3.ExecuteNonQuery()
        End If
        MsgBox("¿Guardar nueva producto?")
        limpiar()
        leer()
    End Sub

    Private Sub leer()
        dclick = False
        Dim consulta As String
        consulta = "SELECT* from ingresos"
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, "ingresos")
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = "ingresos"
        Me.DataGridView1.Columns(0).DefaultCellStyle.Format = "dd/MM/yyyy"
        DataGridView1.Columns(0).Width = 100
        DataGridView1.Columns(1).Width = 218
        DataGridView1.Columns(2).Width = 30
        DataGridView1.Columns(3).Width = 80
        DataGridView1.Columns(4).Width = 69
        DataGridView1.Sort(DataGridView1.Columns(4), System.ComponentModel.ListSortDirection.Descending)
        colorea()
    End Sub
    Private Sub limpiar()
        Label6.Text = ""
        Label8.Text = ""
        Label10.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
    End Sub
    Private Sub Form3_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        limpiar()
    End Sub

    Private Sub calculo() 'suma totales de ingreso y egreso dependiendo de la condición
        Dim total1 As Integer
        Dim total2 As Integer
        Dim desde As String
        Dim hasta As String

        desde = DateTimePicker2.Value.ToString("yyyy-MM-dd")
        hasta = DateTimePicker3.Value.ToString("yyyy-MM-dd")
        Dim sql As String = "select * from ingresos where fecha between'" + desde + "' and '" + hasta + "' "

        Dim adaptador2 As New MySqlDataAdapter(sql, conexion)
        Dim tblFiltro As New DataTable()

        adaptador2.Fill(tblFiltro)
        DataGridView1.DataSource = tblFiltro
        DataGridView1.Sort(DataGridView1.Columns(4), System.ComponentModel.ListSortDirection.Descending)


        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Rows(i).Cells(2).Value = "I" Then
                    total1 += Val(Me.DataGridView1.Rows(i).Cells(3).Value)
                Else
                    total2 += Val(Me.DataGridView1.Rows(i).Cells(3).Value)

            End If
        Next
        MsgBox("Ingresos " & total1 & "  " & "Egresos " & total2)
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If

        Dim renglon As Integer : renglon = e.RowIndex
        DataGridView1.Rows(renglon).Selected = True
        If Not IsDBNull(DataGridView1.Item(2, renglon).Value) Then
            TextBox2.Text = DataGridView1.Item(1, renglon).Value
            TextBox3.Text = DataGridView1.Item(3, renglon).Value
            Label6.Text = DataGridView1.Item(4, renglon).Value
            Dim tipo As String = DataGridView1.Item(2, renglon).Value
            If tipo = "E" Then
                RadioButton2.Checked = True
            Else
                RadioButton1.Checked = True
            End If
            dclick = True
            regis = renglon  'me dice en que reglon guarde'
        End If
        colorea()
    End Sub

    Private Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If dclick = False Then
            MsgBox("haga click en un registro")
        Else
            Dim s As Integer
            s = MsgBox("borra este registro", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                eliminar = "DELETE FROM ingresos WHERE id='" & Label6.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
                limpiar()
                leer()
            End If
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Dim v As String
        v = validar1()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If
        If dclick = False Then
            MsgBox("haga click en un registro")
        Else

            Dim tipo As String = "E"
            If RadioButton1.Checked = True Then
                tipo = "I"
            End If

            Dim actualizar As New MySqlCommand("UPDATE  ingresos SET concepto=@concepto, tipo=@tipo, importe=@importe WHERE id='" & Label6.Text & "'", conexion)
            actualizar.Parameters.AddWithValue("@concepto", TextBox2.Text)
            actualizar.Parameters.AddWithValue("@importe", TextBox3.Text)
            actualizar.Parameters.AddWithValue("@tipo", tipo)

            actualizar.ExecuteNonQuery()
            Dim res As String
            res = Label8.Text
            If res <> "" Then

                Dim tipona As Integer
                tipona = Label10.Text

                If RadioButton1.Checked = True Then
                    tipona = tipona - TextBox4.Text
                End If
                If RadioButton2.Checked = True Then
                    tipona = tipona + TextBox4.Text
                End If

                Dim actualizar3 As New MySqlCommand("UPDATE  stock SET  stock=@stock WHERE id='" & Label8.Text & "'", conexion)

                actualizar3.Parameters.AddWithValue("@stock", tipona)

                actualizar3.ExecuteNonQuery()
            End If
            MsgBox("Producto modificado")
            limpiar()
            leer()
            End If
    End Sub
    Private Function validar1() As String
        validar1 = ""

        If IsNumeric(TextBox3.Text) = False Then
            validar1 &= "No es numerico" & Chr(13)

        End If
        If TextBox2.Text.Length = 0 Then
            validar1 &= "Concepto vacio " & Chr(13)

        End If
    End Function

    Private Function validar() As String
        validar = ""

        If IsNumeric(TextBox3.Text) = False Then
            validar = "No es numerico"
            Exit Function

        End If
        If TextBox2.Text.Length = 0 Then
            validar = "Concepto vacio "
            Exit Function
        End If
    End Function

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        calculo()
    End Sub
    Private Sub colorea() 'colorea celda de Tipo (Ingreso color verde, y rojo cuando no es Egreso)

        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            If Me.DataGridView1.Rows(i).Cells(2).Value = "E" Then
                Me.DataGridView1.Rows(i).Cells(2).Style.BackColor = Color.Red
            ElseIf Me.DataGridView1.Rows(i).Cells(2).Value = "I" Then
                Me.DataGridView1.Rows(i).Cells(2).Style.BackColor = Color.Lime
            End If
        Next
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        leer()
        limpiar()
    End Sub
End Class