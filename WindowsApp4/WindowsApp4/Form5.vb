﻿Imports MySql.Data.MySqlClient

Public Class Form5
    Dim regis As Integer
    Dim dclick As Boolean


    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet
    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=localhost;user=root;password=;database=prueba;Convert Zero Datetime=True"
        conexion.Open()
        Me.Text = "Cliente"
        Label1.Text = Form4.TextBox7.Text
        Label2.Text = ""
        Button1.Text = "Nuevo"
        Button2.Text = "Modificar"
        Button3.Text = "Borrar"
        Button4.Text = "Agendar"

        DataGridView1.RowTemplate.Height = 50
        leer()
        DataGridView1.AllowUserToAddRows = False
    End Sub
    Private Sub leer()

        dclick = False
        Dim consulta As String
        Dim cliente As String


        cliente = Label1.Text
        consulta = "SELECT* from " & cliente
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, cliente)
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = cliente
        DataGridView1.Sort(DataGridView1.Columns(0), System.ComponentModel.ListSortDirection.Descending)

        DataGridView1.Columns(0).Width = 30
        DataGridView1.Columns(1).Width = 75
        DataGridView1.Columns(2).Width = 100
        DataGridView1.Columns(3).Width = 600



    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If

        Dim columna As Integer : columna = e.ColumnIndex ' me dejara hacer click en el data grid haya algo o no 
        Dim renglon As Integer : renglon = e.RowIndex
        If Not IsDBNull(DataGridView1.Item(3, renglon).Value) Then

            RichTextBox1.Text = DataGridView1.Item(3, renglon).Value
            Label2.Text = DataGridView1.Item(0, renglon).Value

            dclick = True
            regis = renglon  'me dice en que reglon guarde'
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim cliente As String
        cliente = Label1.Text
        comandos = New MySqlCommand("INSERT INTO " & cliente & " (fecha,dni,evento)" & Chr(13) &
                                    "VALUES(@fecha,@dni,@evento )", conexion)

        comandos.Parameters.AddWithValue("@evento", RichTextBox1.Text)
        comandos.Parameters.AddWithValue("@fecha", DateTimePicker1.Value)
        comandos.Parameters.AddWithValue("@dni", Label1.Text)
        comandos.ExecuteNonQuery()


        MsgBox("¿Desea guardar nuevo evento?")

        leer()
    End Sub
    Private Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If dclick = False Then
            MsgBox("Haga click en un cliente")
        Else
            Dim s As Integer
            s = MsgBox("¿Borrar evento?", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                Dim cliente As String


                cliente = Label1.Text
                eliminar = "DELETE FROM " & cliente & " WHERE id='" & Label2.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
            End If
        End If
    End Sub
    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click

        If dclick = False Then
            MsgBox("Haga click en un evento")

        Else


            Dim cliente As String

            cliente = Label1.Text

            Dim actualizar As New MySqlCommand("UPDATE " & cliente & " SET evento=@evento WHERE id='" & Label2.Text & "'", conexion)

            actualizar.Parameters.AddWithValue("@evento", RichTextBox1.Text)

            actualizar.ExecuteNonQuery()

            MsgBox("Evento modificado")

            leer()

        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Form6.Show()
    End Sub
End Class