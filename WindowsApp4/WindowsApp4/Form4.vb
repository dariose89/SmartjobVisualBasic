﻿
Imports MySql.Data.MySqlClient
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core

Public Class Form4
    ' datagridview (1) label(3) textbox(3) button(4) radiobutton(2)
    ' este programa pide llenar con los datos y luego los graba en un archivo de texto
    Dim regis As Integer
    Dim dclick As Boolean


    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet
    Private Sub Form4_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=localhost;user=root;password=;database=prueba;Convert Zero Datetime=True"
        conexion.Open()

        Me.Text = "Ingreso de clientes"
        Label1.Text = "Fecha"
        Label2.Text = "Apellido y Nombre"
        Label3.Text = "Equipo"
        Label4.Text = "Tipo de repación"
        Label5.Text = "Presupuesto"
        Label6.Text = "Teléfono"
        Label7.Text = "Dni"
        Label8.Text = "IMEI"
        Label10.Text = ""
        Button1.Text = "Nuevo"
        Button2.Text = "Modificar"
        Button3.Text = "Borrar"
        Button4.Text = "Buscar Dni"
        Button5.Text = "Imprimir Orden"
        CheckBox1.Text = "Hecho"
        CheckBox2.Text = "Entregado"
        CheckBox3.Text = "Sin Arreglo"
        GroupBox1.Text = "Búsqueda Cliente"
        GroupBox2.Text = "Estado"
        leer()
        DataGridView1.AllowUserToAddRows = False
        colorea()
    End Sub


    Private Sub leer()
        dclick = False
        Dim consulta As String
        consulta = "SELECT* from clientes"
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, "clientes")
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = "clientes"

        DataGridView1.Columns(0).Width = 100
        DataGridView1.Columns(1).Width = 200
        DataGridView1.Columns(2).Width = 100
        DataGridView1.Columns(3).Width = 200
        DataGridView1.Columns(4).Width = 100
        DataGridView1.Columns(5).Width = 100
        DataGridView1.Columns(6).Width = 100
        DataGridView1.Columns(7).Width = 150
        DataGridView1.Columns(8).Width = 75
        DataGridView1.Columns(9).Width = 50
        DataGridView1.Sort(DataGridView1.Columns(1), System.ComponentModel.ListSortDirection.Ascending)
        colorea()
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If

        Dim renglon As Integer : renglon = e.RowIndex
        DataGridView1.Rows(renglon).Selected = True
        If Not IsDBNull(DataGridView1.Item(2, renglon).Value) Then
            TextBox2.Text = DataGridView1.Item(1, renglon).Value
            TextBox3.Text = DataGridView1.Item(2, renglon).Value
            TextBox4.Text = DataGridView1.Item(3, renglon).Value
            TextBox5.Text = DataGridView1.Item(4, renglon).Value
            TextBox6.Text = DataGridView1.Item(5, renglon).Value
            TextBox7.Text = DataGridView1.Item(6, renglon).Value
            TextBox8.Text = DataGridView1.Item(7, renglon).Value
            Label10.Text = DataGridView1.Item(9, renglon).Value
            dclick = True
            regis = renglon  'me dice en que reglon guarde'
        End If
        colorea()
    End Sub
    Private Sub limpiar()

        Label10.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        Form5.Show()

    End Sub
    Private Sub Form4_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        limpiar()
    End Sub
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim v As String
        v = validar1()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If

        Dim tipo As String = "P"
        If CheckBox1.Checked = True Then
            tipo = "H"
        ElseIf CheckBox2.Checked = True Then
            tipo = "E"

        End If
        comandos = New MySqlCommand("INSERT INTO clientes (fecha,nombre,equipo,reparacion,presupuesto,tel,dni,imei,estado)" & Chr(13) &
                                    "VALUES(@fecha,@nombre,@equipo,@reparacion,@presupuesto,@tel,@dni,@imei,@estado )", conexion)
        Dim pam As String
        pam = "d" + TextBox7.Text
        comandos.Parameters.AddWithValue("@reparacion", TextBox4.Text)
        comandos.Parameters.AddWithValue("@nombre", TextBox2.Text)
        comandos.Parameters.AddWithValue("@estado", tipo)
        comandos.Parameters.AddWithValue("@equipo", TextBox3.Text)
        comandos.Parameters.AddWithValue("@presupuesto", TextBox5.Text)
        comandos.Parameters.AddWithValue("@tel", TextBox6.Text)
        comandos.Parameters.AddWithValue("@dni", pam)
        comandos.Parameters.AddWithValue("@imei", TextBox8.Text)
        comandos.Parameters.AddWithValue("@fecha", DateTimePicker1.Value)
        comandos.ExecuteNonQuery()

        Dim createSql As String
        Dim tblname As String
        tblname = "d" + TextBox7.Text
        createSql = "CREATE TABLE IF NOT EXISTS " & tblname & "(id INT(6) AUTO_INCREMENT,fecha DATE, dni VARCHAR (50), evento VARCHAR(60000),PRIMARY KEY (id));"
        Dim cmd As New MySqlCommand(createSql, conexion)
        cmd.ExecuteNonQuery()
        cmd.Dispose()

        MsgBox("¿Desea guardar nuevo cliente?")
        limpiar()
        leer()
    End Sub





    Private Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If dclick = False Then
            MsgBox("Haga click en un cliente")
        Else
            Dim s As Integer
            s = MsgBox("¿Borrar cliente?", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                eliminar = "DELETE FROM clientes WHERE id='" & Label10.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
            End If
        End If
        limpiar()
        leer()
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Dim v As String
        v = validar1()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If
        If dclick = False Then
            MsgBox("Haga click en un cliente")

        Else



            Dim tipo As String = "P"
            If CheckBox1.Checked = True Then
                tipo = "H"
            ElseIf CheckBox2.Checked = True Then
                tipo = "E"

            End If
            Dim repar As String = ""
            If CheckBox3.Checked = True Then
                repar = " (SIN ARREGLO)"
            End If
            repar = TextBox2.Text + repar

            Dim actualizar As New MySqlCommand("UPDATE  clientes SET reparacion=@reparacion, nombre=@nombre, estado=@estado, equipo=@equipo, presupuesto=@presupuesto, tel=@tel, dni=@dni, imei=@imei WHERE id='" & Label10.Text & "'", conexion)

                actualizar.Parameters.AddWithValue("@reparacion", TextBox4.Text)
            actualizar.Parameters.AddWithValue("@nombre", repar)
            actualizar.Parameters.AddWithValue("@estado", tipo)
                actualizar.Parameters.AddWithValue("@equipo", TextBox3.Text)
                actualizar.Parameters.AddWithValue("@presupuesto", TextBox5.Text)
                actualizar.Parameters.AddWithValue("@tel", TextBox6.Text)
                actualizar.Parameters.AddWithValue("@dni", TextBox7.Text)
                actualizar.Parameters.AddWithValue("@imei", TextBox8.Text)
                actualizar.ExecuteNonQuery()

                MsgBox("Producto modificado")
                limpiar()
                leer()

            End If

    End Sub
    Private Function validar1() As String
        validar1 = ""



        If IsNumeric(TextBox6.Text) = False Then
            validar1 &= "Teléfono debe ser numérico" & Chr(13)

        End If



        If TextBox2.Text.Length = 0 Then
            validar1 &= "Ingrese apellido y nombre " & Chr(13)

        End If
        If TextBox3.Text.Length = 0 Then
            validar1 &= "Ingrese equipo" & Chr(13)

        End If
        If TextBox4.Text.Length = 0 Then
            validar1 &= " Ingrese tipo de reparación " & Chr(13)

        End If
        If TextBox6.Text.Length = 0 Then
            validar1 &= "Ingrese teléfono " & Chr(13)

        End If
        If TextBox7.Text.Length = 0 Then
            validar1 &= "Ingrese dni " & Chr(13)

        End If


    End Function

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Rows(i).Cells(6).Value = TextBox9.Text Then

                DataGridView1.Rows(i).Selected = True
            End If
        Next

        Dim consulta As String
        Dim lista As Byte
        If TextBox9.Text <> "" Then
            consulta = "SELECT * FROM clientes WHERE dni ='" & TextBox9.Text & "'"
            adaptador = New MySqlDataAdapter(consulta, conexion)
            datos = New DataSet
            adaptador.Fill(datos, "clientes")
            lista = datos.Tables("clientes").Rows.Count
        End If
        If lista <> 0 Then
            TextBox4.Text = datos.Tables("clientes").Rows(0).Item("reparacion")
            TextBox2.Text = datos.Tables("clientes").Rows(0).Item("nombre")
            TextBox3.Text = datos.Tables("clientes").Rows(0).Item("equipo")
            TextBox5.Text = datos.Tables("clientes").Rows(0).Item("presupuesto")
            TextBox6.Text = datos.Tables("clientes").Rows(0).Item("tel")
            TextBox7.Text = datos.Tables("clientes").Rows(0).Item("dni")
            TextBox8.Text = datos.Tables("clientes").Rows(0).Item("imei")

        Else
            MsgBox("datos no encontrados")
        End If

    End Sub

    Private Sub colorea() 'colorea celda de Estado)

        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            If Me.DataGridView1.Rows(i).Cells(8).Value = "P" Then
                Me.DataGridView1.Rows(i).Cells(8).Style.BackColor = Color.Red
            ElseIf Me.DataGridView1.Rows(i).Cells(8).Value = "H" Then
                Me.DataGridView1.Rows(i).Cells(8).Style.BackColor = Color.Yellow
            ElseIf Me.DataGridView1.Rows(i).Cells(8).Value = "E" Then
                Me.DataGridView1.Rows(i).Cells(8).Style.BackColor = Color.Lime
            End If
        Next
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim objWord As New Word.Application
        objWord = New Word.Application
        objWord.Visible = True

        Dim objDocument As New Word.Document

        ''objDocument = objWord.Documents.Open("C:\Users\Administrador\Desktop\Orden Blanco.docx")
        objWord.Documents.Open("C:\Users\Administrador\Desktop\Orden Blanco.docx")
        objWord.Visible = True


        objWord.Documents(1).Bookmarks("id").Range.Text = Label10.Text

        objWord.Documents(1).Bookmarks("fecha").Range.Text = DateTimePicker1.Value

        objWord.Documents(1).Bookmarks("nombre").Range.Text = TextBox2.Text

        objWord.Documents(1).Bookmarks("equipo").Range.Text = TextBox3.Text

        objWord.Documents(1).Bookmarks("reparacion").Range.Text = TextBox4.Text

        objWord.Documents(1).Bookmarks("presupuesto").Range.Text = TextBox5.Text

        objWord.Documents(1).Bookmarks("telefono").Range.Text = TextBox6.Text

        objWord.Documents(1).Bookmarks("dni").Range.Text = TextBox7.Text


    End Sub
End Class