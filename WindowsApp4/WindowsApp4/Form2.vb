﻿
Imports MySql.Data.MySqlClient
Public Class Form2
    ' datagridview (1) label(5) textbox(5) button(5) radiobutton(4)
    ' ABM Ficha de stock con grabación de datos
    Dim regis As Integer
    Dim dclick As Boolean

    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet
    Private Sub Form2_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=localhost;user=root;password=;database=prueba;Convert Zero Datetime=True"
        conexion.Open()
        Me.Text = "Stock de productos"
        Label1.Text = "Costo"
        Label2.Text = "Producto"
        Label3.Text = "Precio"
        Label4.Text = "Stock"
        Label5.Text = "Porcentaje"
        Button1.Text = "Nuevo"
        Button2.Text = "Modificar"
        Button3.Text = "Borrar"
        'Button4.Text = "Calculos"
        Button5.Text = "Ajuste"
        GroupBox1.Text = "Actualizar costo/precio"
        GroupBox2.Text = "Registro"
        RadioButton1.Text = "ingreso"
        RadioButton2.Text = "Egreso"
        RadioButton3.Text = "Precio"
        RadioButton4.Text = "Costo"
        Label6.Text = ""



        leer()
        DataGridView1.AllowUserToAddRows = False
        colorea()
    End Sub





    Private Sub colorea() 'colorea celda de stock en estado crítico (color amarillo menor a 3 unidades, y rojo cuando no hay stock)

        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            If Me.DataGridView1.Rows(i).Cells(3).Value <= 0 Then
                Me.DataGridView1.Rows(i).Cells(3).Style.BackColor = Color.Red
            ElseIf Me.DataGridView1.Rows(i).Cells(3).Value <= 3 Then
                Me.DataGridView1.Rows(i).Cells(3).Style.BackColor = Color.Yellow
            End If
        Next
    End Sub
    Private Sub limpiar()
        Label6.Text = ""
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""

    End Sub
    Private Sub Form2_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        limpiar()
    End Sub
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim v As String
        v = validar()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If

        comandos = New MySqlCommand("INSERT INTO stock (costo,producto,precio,stock)" & Chr(13) &
                                    "VALUES(@costo,@producto,@precio,@stock )", conexion)

        comandos.Parameters.AddWithValue("@costo", TextBox1.Text)
        comandos.Parameters.AddWithValue("@producto", TextBox2.Text)
        comandos.Parameters.AddWithValue("@precio", TextBox3.Text)
        comandos.Parameters.AddWithValue("@stock", TextBox4.Text)
        comandos.ExecuteNonQuery()


        MsgBox("¿Guardar nueva producto?")
        limpiar()
        leer()
        colorea()

    End Sub

    Private Sub leer()
        dclick = False
        Dim consulta As String
        consulta = "SELECT* from stock"
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, "stock")
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = "stock"

        DataGridView1.Columns(0).Width = 100
        DataGridView1.Columns(1).Width = 200
        DataGridView1.Columns(2).Width = 100
        DataGridView1.Columns(3).Width = 100
        DataGridView1.Columns(4).Width = 100
        DataGridView1.Sort(DataGridView1.Columns(1), System.ComponentModel.ListSortDirection.Ascending)

    End Sub


    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If

        Dim renglon As Integer : renglon = e.RowIndex
        DataGridView1.Rows(renglon).Selected = True
        If Not IsDBNull(DataGridView1.Item(2, renglon).Value) Then
            TextBox1.Text = DataGridView1.Item(0, renglon).Value
            TextBox2.Text = DataGridView1.Item(1, renglon).Value
            TextBox3.Text = DataGridView1.Item(2, renglon).Value
            TextBox4.Text = DataGridView1.Item(3, renglon).Value
            Label6.Text = DataGridView1.Item(4, renglon).Value
            dclick = True
            regis = renglon  'me dice en que reglon guarde'

        End If
        colorea()
    End Sub



    Private Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If dclick = False Then
            MsgBox("Haga click en un producto")
        Else
            Dim s As Integer
            s = MsgBox("¿Borrar producto?", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                eliminar = "DELETE FROM stock WHERE id='" & Label6.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
                limpiar()
                leer()
            End If
        End If
        colorea()
    End Sub



    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Dim v As String
        v = validar1()
        If Len(v) > 0 Then
            MsgBox(v)
            Exit Sub
        End If
        If dclick = False Then
            MsgBox("Haga click en un producto")

        Else
            Dim tipo As Integer
            tipo = DataGridView1.Item(3, regis).Value

            If RadioButton1.Checked = True Then
                tipo = tipo + TextBox4.Text
            End If
            If RadioButton2.Checked = True Then
                tipo = tipo - TextBox4.Text
            End If

            Dim actualizar As New MySqlCommand("UPDATE  stock SET costo=@costo, producto=@producto, precio=@precio, stock=@stock WHERE id='" & Label6.Text & "'", conexion)
                actualizar.Parameters.AddWithValue("@costo", TextBox1.Text)
                actualizar.Parameters.AddWithValue("@producto", TextBox2.Text)
                actualizar.Parameters.AddWithValue("@precio", TextBox3.Text)
                actualizar.Parameters.AddWithValue("@stock", tipo)

                actualizar.ExecuteNonQuery()

                MsgBox("Producto modificado")
            limpiar()
            leer()
                colorea()
            End If

    End Sub

    ' Private Sub Button4_Click(sender As Object, e As EventArgs)
    '   suma()
    '   suma_costo()
    '   suma_precio()
    '   medir()
    ' End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        If RadioButton3.Checked = True Then

            Dim actuali As New MySqlCommand("UPDATE  stock SET precio= precio*1." & TextBox5.Text, conexion)
            actuali.ExecuteNonQuery()

        End If
        If RadioButton4.Checked = True Then

            Dim actuali As New MySqlCommand("UPDATE  stock SET costo= costo*1." & TextBox5.Text, conexion)
            actuali.ExecuteNonQuery()


        End If
        leer()
        colorea()
    End Sub
    Private Function validar1() As String
        validar1 = ""
        If TextBox1.Text.Length = 0 Then
            validar1 &= "Ingrese costo " & Chr(13)
            Exit Function
        ElseIf IsNumeric(TextBox1.Text) = False Then
            validar1 &= "No es numerico" & Chr(13)
            Exit Function
        End If
        If TextBox3.Text.Length = 0 Then
            validar1 &= "Ingrese precio " & Chr(13)
            Exit Function
        ElseIf IsNumeric(TextBox3.Text) = False Then
            validar1 &= "No es numerico" & Chr(13)
            Exit Function
        End If
        If TextBox4.Text.Length = 0 Then
            validar1 &= "Ingrese stock " & Chr(13)
            Exit Function
        ElseIf IsNumeric(TextBox4.Text) = False Then
            validar1 &= "No es numerico" & Chr(13)
            Exit Function
        End If
        If TextBox2.Text.Length = 0 Then
            validar1 &= "Ingrese producto " & Chr(13)
            Exit Function
        End If
        If RadioButton1.Checked = False And RadioButton2.Checked = False Then
            MsgBox("Debe seleccionar si es Ingreso o Egreso")
            Exit Function
        End If
    End Function

    Private Function validar() As String
        validar = ""
        If TextBox1.Text.Length = 0 Then
            validar &= "Ingrese costo " & Chr(13)
            Exit Function
        ElseIf IsNumeric(TextBox1.Text) = False Then
            validar = "No es numerico"
            Exit Function
        End If
        If TextBox3.Text.Length = 0 Then
            validar &= "Ingrese precio " & Chr(13)
            Exit Function
        ElseIf IsNumeric(TextBox3.Text) = False Then
            validar = "No es numerico"
            Exit Function

        End If
        If TextBox4.Text.Length = 0 Then
            validar &= "Ingrese stock " & Chr(13)
            Exit Function
        ElseIf IsNumeric(TextBox4.Text) = False Then
            validar = "No es numerico"
            Exit Function

        End If
        If TextBox2.Text.Length = 0 Then
            validar = "Ingrese producto "
            Exit Function
        End If
        If RadioButton2.Checked = True Then
            MsgBox("No puede haber egreso de un producto nuevo")
            Exit Function
        End If
    End Function

End Class